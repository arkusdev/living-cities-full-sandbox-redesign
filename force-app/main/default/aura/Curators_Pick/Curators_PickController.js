/**
 * Created by Robert on 12/21/2017.
 */
({
    doInit : function(component){
          var action = component.get("c.getCuratedArticles");

          action.setParams({
              groupId: component.get("v.recordId")
          });

          action.setCallback(this, function(val){
               var ret = val.getReturnValue();
                component.set("v.curatorPicks", ret);
                console.log(ret);
          });

        $A.enqueueAction(action);
    }
})