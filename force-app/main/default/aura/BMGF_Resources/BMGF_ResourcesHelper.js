/**
 * Created by Robert on 1/23/2018.
 */
({
    loadResources : function(component){
        var action = component.get("c.getResources");
            var isGroup = component.get("v.isGroupPage")=='true';
            action.setParams({
                recordId: component.get("v.recordId"),
                parentObjectName: component.get("v.parentObjectName"),
                childObjectName: component.get("v.childObjectName"),
                junctionObjectName: component.get("v.junctionObjectName"),
                parentRelationshipField: component.get("v.parentRelationField"),
                childRelationshipField: component.get("v.childRelationField"),
                parentSearchExpr: component.get("v.parentSearchExpr"),
                childSearchExpr: component.get("v.childSearchExpr"),
                junctionSearchExpr: component.get("v.junctionSearchExpr"),
                photoVideoUrlField: component.get("v.photoVideoUrlField"),
                descriptionField: component.get("v.descriptionField"),
                groupByFields: component.get("v.groupByFields"),
                orderBy: component.get("v.orderBy"),
                isGroupPage : isGroup,
                groupNameField : component.get("v.groupNameField")
            });
            action.setCallback(this, function(val) {

                var state = val.getState();

                if (state === "SUCCESS") {
                    var resourceList = val.getReturnValue();
                    if (resourceList != null && resourceList.length > 0) {
                        component.set("v.resourceList", resourceList);
                        component.set("v.resourceListSize", resourceList.length);
                        //call callback method if defined
                        var actionCallBack = component.get("v.callBackNotEmptyResources");
                        if (actionCallBack != null)
                            $A.enqueueAction(actionCallBack);

                    } else {

                        //call callback method if defined
                        var actionCallBack = component.get("v.callBackEmptyResources");
                        if (actionCallBack != null)
                            $A.enqueueAction(actionCallBack);


                    }

                } else if (state === "ERROR") {
                    var errors = val.getError();
                    var errMsg = '';
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });

            $A.enqueueAction(action);
    }
})