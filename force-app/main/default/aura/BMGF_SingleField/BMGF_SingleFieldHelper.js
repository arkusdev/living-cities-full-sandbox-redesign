/**
 * Created by robert.truitt on 2/9/2017.
 */
({
    getSingleField: function(component) {
   try {
            var action = component.get("c.getSingleField");
            action.setParams({
                recordId: component.get("v.recordId"),
                objectName : component.get("v.objectName"),
                fieldName: component.get("v.fieldName")
            });

            action.setCallback(this, function(response) {
                var state = response.getState();

                if (state === "SUCCESS") {
                    var fieldValue = response.getReturnValue();
                    if (fieldValue != null) {
                        // debugger;
                        component.set("v.fieldValue", fieldValue);
                    }
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log(e.toString());
        }
    }
})