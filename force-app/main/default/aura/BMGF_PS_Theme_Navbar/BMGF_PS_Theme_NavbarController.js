({
  onClick : function(component, event, helper) {
        var id = event.target.dataset.menuItemId;
        if (id) {
            component.getSuper().navigate(id);
            component.set("v.activeNavBtn", parseInt(id));
         }
   },

   navClick : function(component, event, helper) {
      var navClick = component.getEvent("navClick");
      navClick.setParams({
        "clicked" : true
      });
      navClick.fire();
   }
})