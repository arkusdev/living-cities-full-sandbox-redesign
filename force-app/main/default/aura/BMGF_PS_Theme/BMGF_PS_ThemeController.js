({
	toggleMenu : function(cmp, event, helper) {
		var navBar = cmp.find('navigationBar');
		var menuButton = cmp.find('menuBtn');
		$A.util.toggleClass(navBar, 'hideNav');
		$A.util.toggleClass(menuButton, 'menuActive');
	},

	addClickListener : function(cmp, event, helper) {
		$(document).ready(function(){
    		$(document).on('click touchstart', function(event) {
    			var isNav = false;

        		if ($(event.target).parents().hasClass("menuBtn") == true) {
        			isNav = true;
        		}

        		if (isNav == false) {
					var navBar = cmp.find('navigationBar');
					var menuButton = cmp.find('menuBtn');
					$A.util.addClass(navBar, 'hideNav');
					$A.util.removeClass(menuButton, 'menuActive');
				}
    		});
		});
	},

	doInit : function(component, event, helper) {
		helper.setPage(component);
	}
})