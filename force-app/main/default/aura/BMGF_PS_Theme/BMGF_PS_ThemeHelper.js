({
	addTouchEventListener : function(component) {
		console.log(component);
		$(component.element[0]).on('click', function() {
			console.log(event.target);
		});
	},

	setActiveNav : function(comp) {
		var currentPageUrl = window.location.href;
		function stripQueryStringAndHashFromPath(url) {
  			return url.split("?")[0].split("#")[0];
		}

		currentPageUrl = stripQueryStringAndHashFromPath(currentPageUrl).substring(currentPageUrl.lastIndexOf("/s/")+3);
		if (currentPageUrl.indexOf("/") >= 0) {
			currentPageUrl = currentPageUrl.substring(0, currentPageUrl.indexOf("/"));
		}
	},

	setPage : function(component) {
		var currentPageUrl = window.location.href;
		function stripQueryStringAndHashFromPath(url) {
  			return url.split("?")[0].split("#")[0];
		}

		currentPageUrl = stripQueryStringAndHashFromPath(currentPageUrl).substring(currentPageUrl.lastIndexOf("/s/")+3);
		if (currentPageUrl.indexOf("/") >= 0) {
			currentPageUrl = currentPageUrl.substring(0, currentPageUrl.indexOf("/"));
		}

		switch (currentPageUrl) {
			case "":
				component.set("v.activeNav", 0);
				break;
			case "group":
				component.set("v.activeNav", 1);
				break;
			case "topiccatalog":
				component.set("v.activeNav", 2);
				break;
			case "topic":
				component.set("v.activeNav", 2);
				break;
			case "article":
				component.set("v.activeNav", 2);
				break;
			case "learning-logs":
				component.set("v.activeNav", 3);
				break;
			case "answers":
				component.set("v.activeNav", 4);
				break;
			case "question":
				component.set("v.activeNav", 4);
				break;
			case "bookmarks":
				component.set("v.activeNav", 5);
				break;
			case "messages":
				component.set("v.activeNav", 6);
				break;
		}
	}
})