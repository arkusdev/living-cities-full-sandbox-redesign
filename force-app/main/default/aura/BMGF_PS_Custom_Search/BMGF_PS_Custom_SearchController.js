({
    doSearchKeyUp : function(component, event, helper){
         var replaceAll = function(str, f, r) {
                    return str.split(f).join(r);
                }
         if(event.getParams().keyCode == 13){
            var searchString = component.get("v.searchString");
            if(searchString!=null){
                searchString = replaceAll(searchString, ' ', '+');
                var communityName = window.location.pathname.split('/')[1];
                window.location.href="/" + communityName + "/s/global-search/" + searchString;
            }
        }
    },
    
    doSearch : function(component, event, helper) {

        var searchString = component.get("v.searchString");
        if(searchString!=null){
            var communityName = window.location.pathname.split('/')[1];
            window.location.href="/" + communityName + "/s/global-search/" + searchString;
        }
    },

    afterScriptsLoaded : function(component, event, helper) {
        component.set("v.jsLoaded", true);

        helper.activateTypeahead(component);
    }
})