({
	rerender: function(component, helper) {
        this.superRerender();
        //helper.activateTypeahead(component);
    },

    afterRender: function(component, helper) {
    	this.superAfterRender();
    	helper.activateTypeahead(component);
    }
})