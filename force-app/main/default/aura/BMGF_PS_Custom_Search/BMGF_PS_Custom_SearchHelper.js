({
	activateTypeahead : function(component) {
		var isJSLoaded = component.get("v.jsLoaded");
		/* REMOVE THESE COMMENTS ONCE DEVELOPMENT FOR THIS STARTS AGAIN */
		/* var states = ['Alabama', 'A really long name for an article so we can test how long this can be and how it looks', 'Alaska', 'Arizona', 'Arkansas', 'California',
  'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
  'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
  'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
  'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
  'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
		]; */

		var states = []; 

		if(isJSLoaded){
			var typeaheadActive = $(component.elements[0]).find('.twitter-typeahead').length !== 0;
			var comp = component.elements[0]
			if (!typeaheadActive) {
				$('.searchInputHide .uiInputText').typeahead({
					hint: true,
					highlight: true,
					minLength: 1
				},
				{
  					name: 'states',
					source: this.substringMatcher(states)
				});
			}
		}
	},

	substringMatcher: function(strs) {
		return function findMatches(q, cb) {
			var matches, substringRegex;

		    // an array that will be populated with substring matches
		    matches = [];

		    // regex used to determine if a string contains the substring `q`
    		substrRegex = new RegExp(q, 'i');

    		// iterate through the pool of strings and for any string that
    		// contains the substring `q`, add it to the `matches` array
    		$.each(strs, function(i, str) {
    			if (substrRegex.test(str)) {
    				matches.push(str);
				}
			});

    		cb(matches);
		};
	}
})