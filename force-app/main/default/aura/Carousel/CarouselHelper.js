/**
 * Created by Robert on 12/11/2017.
 */
({
    startCarousel: function(component) {
        var ccItems = component.get("v.carouselItems");

        if (!$A.util.isEmpty(ccItems)) {
            var jsLoaded = component.get("v.jsLoaded");
            var swipeLoaded = component.get("v.swipeCarouselLoaded");

            if (jsLoaded) {
                if (!swipeLoaded) {
                    var slidesToShow = component.get("v.slidesToShow");
                    var swiperOptions = {loop: true};

                    if (ccItems.length>1)
                    {
                        if(slidesToShow>1) {
                            swiperOptions.slidesPerView = slidesToShow;
                            swiperOptions.spaceBetween = 5;
                        }
                        swiperOptions.navigation = {
                            nextEl: component.find('nextBtn').getElement(),
                            prevEl: component.find('prevBtn').getElement(),
                        };
                        swiperOptions.pagination = {
                            el: component.find('navDots').getElement(),
                            clickable: true,
                            renderBullet: function(index, className) {
                                return '<span class="' + className + '"></span>';
                            }
                        };
                        var autoPlayInterval = component.get("v.autoPlayInterval");
                        if (!$A.util.isEmpty(autoPlayInterval)) swiperOptions.autoplay = {
                            delay: autoPlayInterval
                        };
                    }


                    var swiper = new Swiper(component.find('carouselContainer').getElement(), swiperOptions);
                    component.set("v.swipeCarouselLoaded", true);
                }
            }
        }
    }
})