/**
 * Created by robert.truitt@slalom.com on 2/6/2017.
 */
({
    doInit: function(c, event, helper) {

        var action = c.get("c.getCarouselItems");
        var isGroup = c.get("v.isGroupPage") == 'true';
        action.setParams({
            recordId: c.get("v.recordId"),
            parentObjectName: c.get("v.parentObjectName"),
            childObjectName: c.get("v.childObjectName"),
            junctionObjectName: c.get("v.junctionObjectName"),
            parentRelationshipField: c.get("v.parentRelationField"),
            childRelationshipField: c.get("v.childRelationField"),
            imageField: c.get("v.imageField"),
            parentSearchExpr: c.get("v.parentSearchExpr"),
            childSearchExpr: c.get("v.childSearchExpr"),
            junctionSearchExpr: c.get("v.junctionSearchExpr"),
            captionField: c.get("v.captionField"),
            linkField: c.get("v.linkField"),
            orderBy: c.get("v.orderBy"),
            isGroupPage: isGroup,
            groupNameField: c.get("v.groupNameField"),
            isVideoField: c.get("v.isVideoField"),
            isEmbeddedField: c.get("v.isEmbeddedField")
        });

        action.setCallback(this, function(val) {
            var state = val.getState();

            if (state === "SUCCESS") {
                var carouselContainer = val.getReturnValue();
                console.log(carouselContainer);
                if (carouselContainer.carouselParentItems != null && carouselContainer.carouselParentItems.length > 0) {
                    var carouselItems = [];
                    carouselContainer.carouselParentItems.forEach(function(parItem) {
                        parItem.carouselItems.forEach(function(item) {
                            carouselItems.push(item);
                        });
                    });
                    c.set("v.carouselItems", carouselItems);

                    var emptyEvent = c.getEvent("emptyCarousel");
                    emptyEvent.setParams({
                        "isCarouselEmpty": false
                    });
                    emptyEvent.fire();

                    //call callback method if defined
                    var actionCallBack = c.get("v.callBackNotEmptyCarousel");
                    if (actionCallBack != null)
                        $A.enqueueAction(actionCallBack);

                } else {
                    var carouselContainer = c.find('cc');
                    $A.util.addClass(carouselContainer, 'slds-hide');

                    //register event
                    var emptyEvent = c.getEvent("emptyCarousel");
                    emptyEvent.setParams({
                        "isCarouselEmpty": true
                    });
                    emptyEvent.fire();

                    //call callback method if defined
                    var actionCallBack = c.get("v.callBackEmptyCarousel");
                    if (actionCallBack != null)
                        $A.enqueueAction(actionCallBack);
                }
            }

        });

        $A.enqueueAction(action);
    },
    doInitJS: function(c, event, helper) {
        $.noConflict(); if (typeof(window.$) === 'undefined') { window.$ = jQuery; }
        c.set("v.jsLoaded", true);
    },
    startCarousel : function(c, event, helper){
        helper.startCarousel(component);
    }
})