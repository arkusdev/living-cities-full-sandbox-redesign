/**
 * Created by robert.truitt@slalom.com on 12/11/2017.
 */
({
    rerender: function(component, helper) {
        this.superRerender();
        helper.startCarousel(component);
    }
})