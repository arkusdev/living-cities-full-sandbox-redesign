/**
 * Created by robert.truitt on 12/27/2016.
 */
({
    doInit: function(component, event, helper) {
        debugger;
        var objectNameString = component.get("v.objectName");
        var action;
        if (component.get("v.objectName") != null && component.get("v.objectName").length > 0) {
            action = component.get("c.getLayoutByObject");
            action.setParams({
                objectName: objectNameString,
                recordId: component.get("v.recordId"),
                daysFromTodayNew: component.get("v.daysFromTodayNew")
            });
        } else {
            action = component.get("c.getLayout");
            action.setParams({
                urlName: component.get("v.urlName")
            });
        }

        action.setCallback(this, function(val) {
            var layoutObj = val.getReturnValue();
            console.log(layoutObj);
            if (layoutObj != null) {
                component.set("v.layoutObject", layoutObj);
                component.set("v.styleTag", layoutObj.styleTag);
                component.set("v.isFollowed", layoutObj.isFollowing);
                if (layoutObj.articleList != null) {
                    component.set("v.topicArticles", layoutObj.articleList);
                }
                var fieldValuesMap = {};
                var fieldMapping = layoutObj.kbaLayoutItems;
                for (i in fieldMapping) {
                    if (fieldMapping[i].fieldName != null && fieldMapping[i].fieldName.length > 0) {
                        fieldValuesMap[fieldMapping[i].fieldName] = fieldMapping[i].value;
                    }
                }
                component.set("v.fieldMap", fieldValuesMap);
            }
        })
        $A.enqueueAction(action);
    },
    nextPageTopicArticles: function(component, event, helper) {
        component.set("v.articlesStartIndex", component.get("v.articlesStartIndex") + component.get("v.articlesPageSize"));
    },
    prevPageTopicArticles: function(component, event, helper) {
        var prevIndex = component.get("v.articlesStartIndex") - component.get("v.articlesPageSize");

        if (prevIndex < 0) prevIndex = 0;

        component.set("v.articlesStartIndex", prevIndex);
    },
    handleSortMenu: function(component, event, helper) {
        var sortBy = event.getParam("value");
        var sortByLabel;
        switch (sortBy) {
            case "Title":
                sortByLabel = "Name";
                break;
            case "FirstPublishedDate":
                sortByLabel = "Most Recent";
                sortBy = 'FirstPublishedDate Desc'
                break;
            default:
                sortByLabel = "Name";
                break;
        }
        component.set("v.sortedByField", sortByLabel);
        helper.getTopicArticles(component, sortBy);
    },
    handleFollow: function(component, event, helper) {
        var isFollowed = component.get("v.isFollowed");
        action = component.get("c.toggleFollow");
        action.setParams({
            recordId: component.get("v.recordId"),
            isFollowing: isFollowed
        });
        $A.enqueueAction(action);
        component.set("v.isFollowed", !isFollowed);
    }
})