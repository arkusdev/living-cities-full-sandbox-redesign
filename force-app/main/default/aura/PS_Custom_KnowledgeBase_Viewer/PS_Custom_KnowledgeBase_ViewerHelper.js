/**
 * Created by robert.truitt@Slalom on 5/9/2017.
 */
({
    getTopicArticles: function(component, sortBy) {
        action = component.get("c.getTopicArticles");
        action.setParams({
            recordId: component.get("v.recordId"),
            daysFromTodayNew: component.get("v.daysFromTodayNew"),
            orderBy: sortBy
        });
        action.setCallback(this, function(val) {
            var topicArticles = val.getReturnValue();
            if (topicArticles != null) {
                component.set("v.articlesStartIndex", 0);
                component.set("v.topicArticles", topicArticles);
            }
        });

        $A.enqueueAction(action);
    }
})