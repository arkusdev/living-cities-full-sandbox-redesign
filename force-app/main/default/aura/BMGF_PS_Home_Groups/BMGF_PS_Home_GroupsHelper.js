({
	setActiveGroupList : function(children, setActiveGroup) {
		for (var i=0; i<children.length;i++) {
			// Ideally, this would go into its own function to make it more scalable, but for time efficiency sake will put everything on this function
			if (!setActiveGroup) {
				if (children[i].classList != null && children[i].classList.contains("active")) {
					children[i].classList.remove("active");
				}
			} else {
				if (children[i].attributes != null) {
					for (var j=0; j<children[i].attributes.length; j++) {
						if (children[i].attributes[j].name == "data-group-type" && children[i].attributes[j].nodeValue == setActiveGroup) {
							children[i].classList.add("active");
						}
					}
				}
			}

			// Look inside current node's children
			if (children[i].childNodes != null && children[i].childNodes.length > 0) {
				this.setActiveGroupList(children[i].childNodes, setActiveGroup);
			}
		}
	},

	toggleVisibleGroups : function(children) {
		//console.log(children);
	}
})