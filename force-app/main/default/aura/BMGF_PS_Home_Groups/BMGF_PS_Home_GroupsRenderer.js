({
	afterRender: function (component, helper) {
    	this.superAfterRender();
    	if (component.elements[0].childNodes != null && component.elements[0].childNodes.length > 0) {
    		helper.toggleVisibleGroups(component.elements[0].childNodes);
    	}
	},
})