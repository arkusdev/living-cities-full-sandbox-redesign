@IsTest
public with sharing class BMGF_Resource_Controller_Test {
    @isTest(SeeAllData=false)
    static void testResourceController() {

        CollaborationGroup cg = new CollaborationGroup(Name = 'Test Group', CollaborationType = 'Public');
        insert cg;

        CMS_Site__c cmsSite = new CMS_Site__c();
        cmsSite.Name = 'PS Community';
        insert cmsSite;
        CMS_Page_Group__c cmsGroup = new CMS_Page_Group__c();
        cmsGroup.Page_Group_Name__c = 'est Group';
        cmsGroup.CMS_Site__c = cmsSite.Id;
        insert cmsGroup;

        CMS_Page_Group_Resource__c cmsGrpResource = new CMS_Page_Group_Resource__c();
        //psGrpResource = 'Test Resource';
        cmsGrpResource.Type__c = 'Feature Carousel';
        cmsGrpResource.External_url__c = 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png';
        cmsGrpResource.Caption__c = 'Google\'s photo';
        cmsGrpResource.CMS_Page_Group__c = cmsGroup.Id;
        insert cmsGrpResource;

        ContentVersion cv = new ContentVersion();
        cv.ContentURL = 'http://www.google.com/';
        cv.Title = 'Google.com';
        insert cv;

        cv = [Select ContentDocumentId, Id from ContentVersion where Id = :cv.Id];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cv.ContentDocumentId;
        cdl.LinkedEntityId = cmsGrpResource.Id;
        cdl.ShareType = 'I';

        insert cdl;

        BMGF_Resource_Controller.getResources('', 'CMS_Page_Group__c', 'CMS_Page_Group_Resource__c', '', 'CMS_Page_Group__c', '', '', '', '', '', '', 'CMS_Page_Group_Resource__c.Type__c', 'CreatedDate', false, 'Group_Name__c');
        BMGF_Resource_Controller.getResources('', 'CMS_Page_Group__c', 'CMS_Page_Group_Resource__c', '', 'CMS_Page_Group__c', '', '', '', '', 'GP_Resource__c.External_URL__c', '', 'GP_Resource__c.Type__c', 'CreatedDate', false, 'Group_Name__c');
        BMGF_Resource_Controller.getResources(cg.Id, 'CMS_Page_Group__c', 'CMS_Page_Group_Resource__c', '', 'CMS_Page_Group__c', '', '', '', '', 'GP_Resource__c.External_URL__c', '', 'GP_Resource__c.Type__c', 'CreatedDate', true, 'Page_Group_Name__c');
        
    }
}