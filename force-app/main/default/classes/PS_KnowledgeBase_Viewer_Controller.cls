/**
 * Created by robert.truitt on 2/24/2017.
 */

public with sharing class PS_KnowledgeBase_Viewer_Controller {

    /**
    * @description Get Settings for PS Community
    * @return PSSettings__c
    */
    public static PSSettings__c psSettings {
        get {
            if (psSettings == null) {
                psSettings = PSSettings__c.getOrgDefaults();
            }
            return psSettings;
        }
        set;
    }
   /**
    * @description Exception class for no data in field
    */
    private class NoFieldDataException extends Exception {
    }

    public static String communityName {
        get {
            if (String.isEmpty(communityName)) {
                try {
                    communityName = [Select UrlPathPrefix from Network where Id = :Network.getNetworkId() Limit 1].UrlPathPrefix;
                } catch (QueryException qe) {
                    communityName = '';
                }
            }
            return communityName;
        }
        set;
    }

    /**
    * @description Layout class returned to lightning component
    */
    public class KBALayout {
        @AuraEnabled
        public string communityUrlPrefix {
            get {
                return PS_KnowledgeBase_Viewer_Controller.communityName;
            }
        }
        @AuraEnabled
        public String urlName;

        @AuraEnabled
        public String recordId;

        @AuraEnabled
        public String objectType;

        @AuraEnabled
        public String styleTag;

        @AuraEnabled
        public Boolean isTwoColumns;

        @AuraEnabled
        public List<KBALayoutItem> kbaLayoutItems {
            get {
                if (kbaLayoutItems == null) {
                    kbaLayoutItems = new List<KBALayoutItem>();
                }
                return kbaLayoutItems;
            }
            set {
                if (kbaLayoutItems == null) {
                    kbaLayoutItems = new List<KBALayoutItem>();
                }
                kbaLayoutItems = value;
            }
        }
        @AuraEnabled
        public String errMsg;

        public Integer daysFromTodayNew;

        @AuraEnabled
        public List<KBArticle> articleList;

        @AuraEnabled
        public Boolean isFollowing = false;
    }

    public class KBALayoutItem {
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String labelName;
        @AuraEnabled
        public String layoutType;
        @AuraEnabled
        public String value;
        @AuraEnabled
        public String columnLocation;
        @AuraEnabled
        public Boolean sectionHeader;
        @AuraEnabled
        public String customStyle;
        @AuraEnabled
        public String customClass;
        @AuraEnabled
        public String fileFieldUrl;
        @AuraEnabled
        public String lightningIcon;
        @AuraEnabled
        public String fieldIconUrl;
        @AuraEnabled
        public String fontAwesomeITag;
        @AuraEnabled
        public List<KBALayoutItem> subItems;
        @AuraEnabled
        public Integer subItemsLength;

    }

    public class KBArticle {

        @AuraEnabled
        public KnowledgeArticleVersion article;

        @AuraEnabled
        public Boolean isNewArticle;

        @AuraEnabled
        public Date publishedDate;
    }

    /**
    * @description initialize article details such as article _kav type and id
    * @param urlName    url Name of article, spring 2017 seems to implement recordid which I may change at a future
    *                   date
    * @return KBALyaout
    */
    private static KBALayout initArticleDetails(String urlName) {
        KBALayout kbaLayout = new KBALayout();
        kbaLayout.urlName = urlName;
        KnowledgeArticleVersion[] kbVersions = [
                Select Id, Title, UrlName
                from KnowledgeArticleVersion
                where PublishStatus = 'Online' and language = 'en_US' and UrlName = :urlName
                LIMIT 1
        ];

        if (!kbVersions.isEmpty()) {
            String recordId = kbVersions[0].Id;
            String articleType = getArticleType(recordId);
            kbaLayout.recordId = recordId;
            kbaLayout.objectType = articleType;
        }
        return kbaLayout;
    }

    /**
    * @description given an article, returns its type
    * @param articleId  Article Id
    * @return Article Type
    */
    @TestVisible
    private static String getArticleType(String articleId){
        Map<String, String> describeMap = new Map<String, String>();
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Set<String> keySet = gd.keySet();
        for (String key : keySet) {
            Schema.SObjectType objectType = gd.get(key);
            if (key.endsWith('kav') || key.endsWith('kb')) {
                describeMap.put(objectType.getDescribe().getKeyPrefix(), objectType.getDescribe().getName());
            }
        }
        String articlePrefix = articleId.substring(0,3);
        Set<String> keySet2 = describeMap.keySet();
        String articleType = null;
        for(String key: keySet2) {
            if(articlePrefix.equalsIgnoreCase(key)) {
                articleType = describeMap.get(key);
                return articleType;
            }
        }
        return articleType;
    }

    /**
    * @description initialize article details
    * @param urlName    url Name of article, spring 2017 seems to implement recordid which I may change at a future
    *                   date
    * @return KBALyaout
    */
    private static Map<String, Schema.SObjectField> getAllObjFields(String articleType) {
        SObjectType articleSobjType = Schema.getGlobalDescribe().get(articleType);
        System.debug(articleSobjType.getDescribe().fields.getMap());
        return articleSobjType.getDescribe().fields.getMap();
    }

    /**
    * @description exposed method for getting layout by object name such as PSTopics
    * @param objectName   Custom Portal Layout object name field reference
    * @param recordId     RecordId if specified such as with topic articles
    * @return KBALyaout
    */
    @AuraEnabled
    public static KBALayout getLayoutByObject(String objectName, String recordId, Integer daysFromTodayNew) {
        KBALayout kbaLayout = new KBALayout();
        try {
            kbaLayout.recordId = recordId;
            kbaLayout.objectType = objectName;
            if (daysFromTodayNew != null)
                kbaLayout.daysFromTodayNew = Integer.valueOf(daysFromTodayNew);
            getLayoutDetails(kbaLayout);
        } catch (Exception e) {
            kbaLayout.errMsg = e.getMessage();
        }
        return kbaLayout;
    }

    /**
    * @description exposed method for getting layout by urlName, specifically for articles
    * @param urlName    url Name of article, spring 2017 seems to implement recordid which I may change at a future
    *                   date
    * @return KBALyaout
    */
    @AuraEnabled
    public static KBALayout getLayout(String urlName) {
        KBALayout kbaLayout = new KBALayout();
        try {
            kbaLayout = initArticleDetails(urlName);
        } catch (Exception e) {
            kbaLayout.errMsg = e.getMessage();
        }
        return getLayoutDetails(kbaLayout);
    }

    /**
    * @description Main method for building layout from Custom Portal Layout/Custom Portal Layout Item and SubItem objects
    * @param kbaLayout    The kbaLayout class initialized in getLayout/getLayoutByObject methods
    * @return KBALyaout
    */
    @TestVisible
    private static KBALayout getLayoutDetails(KBALayout kbaLayout) {
        List<KBA_Custom_Layout__c> custLayout = new List<KBA_Custom_Layout__c>();
        if (String.isNotEmpty(kbaLayout.objectType)) {
            custLayout = [
                    Select Id, Style_Tag__c, X2_column_layout__c, (
                            select Field_Name__c, Section_Header__c, Sequence__c, Show_Label__c, Custom_Label__c,
                                    Custom_HTML__c, Literal_Value__c, Custom_Style__c, Custom_Class__c, Custom_Type__c,
                                    Column_Location__c, Field_Icon__c
                            from Layout_Items__r
                            Order by Sequence__c
                    )
                    from KBA_Custom_Layout__c
                    where Object_API_Name__c = :kbaLayout.objectType
                    LIMIT 1
            ];
        }
        if (custLayout.isEmpty()) {
            custLayout = [
                    Select Id, Style_Tag__c, X2_column_layout__c, (
                            select Field_Name__c, Section_Header__c, Sequence__c, Show_Label__c, Custom_Label__c,
                                    Custom_HTML__c, Literal_Value__c, Custom_Style__c, Custom_Class__c, Custom_Type__c,
                                    Column_Location__c, Field_Icon__c
                            from Layout_Items__r
                            Order by Sequence__c
                    )
                    from KBA_Custom_Layout__c
                    where Object_API_Name__c = 'Default'
                    LIMIT 1
            ];
        }
        if (!custLayout.isEmpty()) {

            Map<String, List<KBA_Custom_Layout_SubItem__c>> portalItemsSubItems = new Map<String, List<KBA_Custom_Layout_SubItem__c>>();

            for (KBA_Custom_Layout_SubItem__c subItem : [
                    Select Field_Name__c, Show_Label__c, Layout_Parent_Item__c, Custom_Class__c, Custom_Style__c,
                            Field_Icon__c, Custom_Label__c, Custom_HTML__c, Literal_Value__c, Custom_Type__c
                    from KBA_Custom_Layout_SubItem__c
                    where Layout_Parent_Item__c in (Select Id from KBA_Custom_Layout_Item__c where KBA_Custom_Layout__c = :custLayout.get(0).Id)
                    Order by Sequence__c
            ]) {
                List<KBA_Custom_Layout_SubItem__c> subItemList = portalItemsSubItems.get(subItem.Layout_Parent_Item__c);
                if (subItemList == null) {
                    subItemList = new List<KBA_Custom_Layout_SubItem__c>();
                    subItemList.add(subItem);
                    portalItemsSubItems.put(subItem.Layout_Parent_Item__c, subItemList);
                } else {
                    subItemList.add(subItem);
                }
            }


            kbaLayout.styleTag = custLayout.get(0).Style_Tag__c;
            kbaLayout.isTwoColumns = custLayout.get(0).X2_column_layout__c;


            String searchString = '';
            Sobject curObj;

            Map<String, Schema.SObjectField> fieldLabelMap;
            if (String.isNotEmpty(kbaLayout.urlName)) {
                fieldLabelMap = getAllObjFields(kbaLayout.objectType);
                if (kbaLayout.objectType.endsWith('kav')) {
                    searchString = 'Select '
                            + String.join(new List<String>(fieldLabelMap.keySet()), ',') +
                            ' from '
                            + kbaLayout.objectType +
                            ' where Id = \'' + kbaLayout.recordId + '\' LIMIT 1';
                }
                System.debug(searchString);
                curObj = Database.query(searchString);
            }

            for (KBA_Custom_Layout_Item__c layoutItem : custLayout.get(0).Layout_Items__r) {
                KBALayoutItem kbaLayoutItem = new KBALayoutItem();
                try {
                    if (layoutItem.Section_Header__c) {
                        if (String.isNotEmpty(layoutItem.Field_Name__c)) {
                            Schema.DescribeFieldResult sectionHeaderFieldDesc = fieldLabelMap.get(layoutItem.Field_Name__c).getDescribe();
                            String sectionHeaderFieldValue = String.valueOf(curObj.get(sectionHeaderFieldDesc.getName()));
                            kbaLayoutItem.labelName = sectionHeaderFieldValue != null ? sectionHeaderFieldValue : layoutItem.Field_Name__c;
                        } else {
                            kbaLayoutItem.labelName = layoutItem.Literal_Value__c;
                        }
                        List<KBALayoutItem> layoutSubItems = new List<KBALayoutItem>();
                        for (KBA_Custom_Layout_SubItem__c subItem : portalItemsSubItems.get(layoutItem.Id)) {
                            KBALayoutItem kbaLayoutSubItem = new KBALayoutItem();
                            if (String.isNotEmpty(subItem.Field_Name__c)) {
                                try {
                                    Schema.DescribeFieldResult subItemFieldDesc = fieldLabelMap.get(subItem.Field_Name__c).getDescribe();
                                    String subItemValue = String.valueOf(curObj.get(subItemFieldDesc.getName()));
                                    if (String.isNotEmpty(subItemValue)) {
                                        kbaLayoutSubItem.value = subItemValue;
                                        kbaLayoutSubItem.fieldName = subItemFieldDesc.getName();
                                        kbaLayoutSubItem.labelName = subItem.Show_Label__c ? (String.isEmpty(subItem.Custom_Label__c) ? subItemFieldDesc.getLabel() : subItem.Custom_Label__c) : '';
                                        kbaLayoutSubItem.layoutType = subItemFieldDesc.getType().name();
                                    } else {
                                        continue;
                                    }
                                } catch (NullPointerException npe) {
                                    //field doesn't exist
                                    continue;
                                }
                                kbaLayoutSubItem.customStyle = subItem.Custom_Style__c;
                                kbaLayoutSubItem.customClass = subItem.Custom_Class__c;
                                if (String.isNotEmpty(subItem.Field_Icon__c)) {
                                    if (subItem.Field_Icon__c.startsWithIgnoreCase ('fa-')) {
                                        //font awesome icon
                                        kbaLayoutSubItem.fontAwesomeITag = subItem.Field_Icon__c;
                                    } else if (subItem.Field_Icon__c.contains('/')) {
                                        kbaLayoutSubItem.fieldIconUrl = subItem.Field_Icon__c;
                                    } else {
                                        kbaLayoutSubItem.lightningIcon = subItem.Field_Icon__c;
                                    }
                                }
                                if (String.isNotEmpty(subItem.Custom_Type__c) && subItem.Custom_Type__c.equalsIgnoreCase('filefield')) {
                                    kbaLayoutSubItem.layoutType = subItem.Custom_Type__c;
                                    kbaLayoutSubItem.fileFieldUrl = '/' + communityName + '/servlet/fileField?entityId=' + kbaLayout.recordId + '&field=' + kbaLayoutSubItem.fieldName;
                                }
                            } else if (String.isNotEmpty(subItem.Literal_Value__c)) {
                                kbaLayoutSubItem.value = subItem.Literal_Value__c;
                                kbaLayoutSubItem.layoutType = 'TEXT';
                            } else if (String.isNotEmpty(subItem.Custom_HTML__c)) {
                                kbaLayoutSubItem.value = subItem.Custom_HTML__c;
                                kbaLayoutSubItem.layoutType = 'HTML';
                            }
                            layoutSubItems.add(kbaLayoutSubItem);
                        }
                        if (!layoutSubItems.isEmpty()) {
                            kbaLayoutItem.subItems = layoutSubItems;
                            kbaLayoutItem.subItemsLength = layoutSubItems.size();
                        }
                    } else if (String.isNotEmpty(layoutItem.Field_Name__c)) {
                        Schema.DescribeFieldResult fieldDesc = fieldLabelMap.get(layoutItem.Field_Name__c).getDescribe();
                        String stringValue = String.valueOf(curObj.get(fieldDesc.getName()));
                        if (String.isNotEmpty(stringValue)) {
                            kbaLayoutItem.value = stringValue;
                            kbaLayoutItem.fieldName = fieldDesc.getName();
                            kbaLayoutItem.labelName = layoutItem.Show_Label__c ? (String.isEmpty(layoutItem.Custom_Label__c) ? fieldDesc.getLabel() : layoutItem.Custom_Label__c) : '';
                            kbaLayoutItem.layoutType = fieldDesc.getType().name();
                            if (String.isNotEmpty(layoutItem.Custom_Type__c) && layoutItem.Custom_Type__c.equalsIgnoreCase('filefield')) {
                                kbaLayoutItem.fileFieldUrl = '/' + communityName + '/servlet/fileField?entityId=' + kbaLayout.recordId + '&field=' + kbaLayoutItem.fieldName;
                            }
                        } else {
                            throw new NoFieldDataException();
                        }
                    } else if (String.isNotEmpty(layoutItem.Literal_Value__c)) {
                        kbaLayoutItem.value = layoutItem.Literal_Value__c;
                        kbaLayoutItem.layoutType = 'TEXT';
                    } else if (String.isNotEmpty(layoutItem.Custom_HTML__c)) {
                        kbaLayoutItem.value = layoutItem.Custom_HTML__c;
                        kbaLayoutItem.layoutType = 'HTML';
                    } else if (String.isNotEmpty(layoutItem.Custom_Type__c)) {
                        if (layoutItem.Custom_Type__c.equalsIgnoreCase('articletopics')) {
                            kbaLayoutItem.labelName = layoutItem.Custom_Label__c;
                            String topicHTML = '';
                            for (Topic tpc : [Select Id, Name from Topic where Id in (Select TopicId from TopicAssignment where EntityId = :kbaLayout.recordId)]) {
                                topicHTML += '<span class=\'articleTopicTag\'><a href=\'/' + communityName + '/s/topic/' + tpc.Id + '\'>' + tpc.Name + '</a>&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                            }
                            kbaLayoutItem.value = topicHTML;
                            kbaLayoutItem.layoutType = 'HTML';
                        } else if (layoutItem.Custom_Type__c.equalsIgnoreCase('communitytopics')) {

                            kbaLayoutItem.labelName = layoutItem.Custom_Label__c;
                            kbaLayoutItem.value = '<span class=\'topiclistinfo\'>Click on any of the items listed below to find articles in the knowledge base about that topic:</span><hr/>' + String.join(communityTopics(), '');
                            kbaLayoutItem.layoutType = 'HTML';

                        } else if (layoutItem.Custom_Type__c.equalsIgnoreCase('articlebytopiclist')) {
                            //check if following
                            kbaLayout.isFollowing = isFollowing(kbaLayout.recordId);
                            List<Topic> curTopic = [Select Name from Topic where Id = :kbaLayout.recordId];

                            if (curTopic.isEmpty()) {
                                kbaLayoutItem.labelName = layoutItem.Custom_Label__c;
                                kbaLayoutItem.value = 'No articles found for this topic:<hr/>';
                                kbaLayoutItem.layoutType = 'HTML';
                            } else {
                                kbaLayoutItem.labelName = curTopic[0].Name;
                                kbaLayoutItem.value = 'ArticleByTopicList';
                                //should only be one list or articles, setting it at the layout so i don't have to iterate
                                //through items on the client side
                                kbaLayout.articleList = getTopicArticles(kbaLayout.recordId, kbaLayout.daysFromTodayNew);
                                kbaLayoutItem.layoutType = 'ArticleByTopicList';
                            }
                        } else if (layoutItem.Custom_Type__c.equalsIgnoreCase('discussions')) {
                            kbaLayoutItem.labelName = layoutItem.Custom_Label__c;
                            kbaLayoutItem.value = 'chatterfeed';
                            kbaLayoutItem.layoutType = 'HTML';
                        }
                    }

                    kbaLayoutItem.sectionHeader = layoutItem.Section_Header__c;
                    kbaLayoutItem.customStyle = layoutItem.Custom_Style__c;
                    kbaLayoutItem.customClass = layoutItem.Custom_Class__c;
                    kbaLayoutItem.columnLocation = layoutItem.Column_Location__c;
                    kbaLayoutItem.layoutType = String.isEmpty(layoutItem.Custom_Type__c) ? kbaLayoutItem.layoutType : layoutItem.Custom_Type__c;
                    if (String.isNotEmpty(layoutItem.Field_Icon__c)) {
                        if (((String) layoutItem.Field_Icon__c).startsWithIgnoreCase ('fa-')) {
                            //font awesome icon
                            kbaLayoutItem.fontAwesomeITag = layoutItem.Field_Icon__c;
                        } else if (layoutItem.Field_Icon__c.contains('/')) {
                            kbaLayoutItem.fieldIconUrl = layoutItem.Field_Icon__c;
                        } else {
                            kbaLayoutItem.lightningIcon = layoutItem.Field_Icon__c;
                        }
                    }
                } catch (NullPointerException npe) {
                    System.debug(npe);
                    //field doesn't exist
                    continue;
                } catch (NoFieldDataException nfde) {
                    System.debug(nfde);
                    continue;
                }
                kbaLayout.kbaLayoutItems.add(kbaLayoutItem);
            }
            if (kbaLayout != null) {
                quickLinksUrlRewriter(kbaLayout.kbaLayoutItems);
            }
        }

        return kbaLayout;
    }

    public static List<KBArticle> getTopicArticles(String recordId, Integer daysFromTodayNew) {
        return getTopicArticles(recordId, daysFromTodayNew, 'Title');
    }

    @AuraEnabled
    public static List<KBArticle> getTopicArticles(String recordId, Integer daysFromTodayNew, String orderBy) {
        //strange lightning component bug, recasting
        Integer daysFromTodayNewRecast = Integer.valueOf(daysFromTodayNew);
        List<KBArticle> kbArticles = new List<KBArticle>();
        Integer kbTopicPaginationCount = 0;

        Set<Id> kbtopicAssignments = new Set<Id>();
        Boolean moreRecords = true;
        Integer offsetCount = 0;
        while (moreRecords) {
            List<TopicAssignment> tAList = [Select EntityId from TopicAssignment where topicId = :recordId LIMIT 1000 OFFSET :offsetCount];
            for (TopicAssignment tp : tAList) {
                kbtopicAssignments.add(tp.EntityId);
            }
            if (tAList.isEmpty() || tAList.size() < 1000) {
                moreRecords = false;
            } else {
                offsetCount += 1000;
            }
        }
        String kbaSearchString = 'Select Id, urlName, Title, FirstPublishedDate ' +
                'from KnowledgeArticleVersion ' +
                'where Id in :kbtopicAssignments ' +
                'and  publishStatus = \'Online\' AND language = \'en_US\'' +
                'order by ' + orderBy;

        for (KnowledgeArticleVersion kbav : Database.query(kbaSearchString)) {
            KBArticle kbArticle = new KBArticle();
            kbArticle.article = kbav;
            if (kbav.FirstPublishedDate != null && daysFromTodayNewRecast != null) {
                try {
                    Date newAsOfDate = System.today() - daysFromTodayNewRecast;
                    Date articlePublishedDate = Date.newInstance(kbav.FirstPublishedDate.year(), kbav.FirstPublishedDate.month(), kbav.FirstPublishedDate.day());
                    kbArticle.isNewArticle = articlePublishedDate >= newAsOfDate;
                    kbArticle.publishedDate = articlePublishedDate;
                } catch (Exception e) {
                    System.debug(e);
                }
            } else {
                kbArticle.isNewArticle = false;
            }
            kbArticles.add(kbArticle);
        }
        return kbArticles;
    }

    /**
    * @description Get all Community Topics
    * @return List<String> topics a href strings in list format
    */
    public static List<String> communityTopics() {
        List<String> communityTopicList = new List<String>();

        AggregateResult[] topicsWithCounts = [
                Select Count(Topic.Id) articleCount, Topic.Name topicName, TopicId
                from TopicAssignment
                where TopicId in (SELECT Id from Topic where NetworkId = :Network.getNetworkId())
                and EntityId in (
                        SELECT Id
                        FROM KnowledgeArticleVersion
                        WHERE publishStatus = 'Online' AND language = 'en_US'
                )
                group by Topic.Name, TopicId
                order by Topic.Name
        ];
        communityTopicList.add('<div class=\'topiclist\'>');

        for (Integer i = 0; i < topicsWithCounts.size(); i++) {
            String topicHtml = '';
            if (i == 0) topicHtml += '<div style=\'max-width:50%; float:left;\'>';
            topicHtml += '<span style=\'display:block;\'><a class="topiclink" href=\'/' + communityName + '/s/topic/' + topicsWithCounts[i].get('TopicId') + '\' style=\'cursor: pointer;\'>' + topicsWithCounts[i].get('topicName') + ' (' + topicsWithCounts[i].get('articleCount') + ')' + '</a></span>';
            System.debug(i + ' ' + (topicsWithCounts.size() / 2) + ' ' + Math.ceil(((topicsWithCounts.size() / 2))));
            if (i == Math.floor((topicsWithCounts.size() - 1) / 2)) topicHtml += '</div><div style=\'max-width:50%; float:right;\'>';
            if (i == topicsWithCounts.size() - 1) topicHtml += '</div><div style=\'clear:both;\'/>';
            communityTopicList.add(topicHTML);
        }
        communityTopicList.add('</div>');
        return communityTopicList;
    }

    //url rewriter for quick links issue
    private static void quickLinksUrlRewriter(List<KBALayoutItem> kbaItems) {
        String preString = psSettings.KBA_URL_Rewrite_Internal__c;
        String postString = psSettings.KBA_URL_Rewrite_External__c;

        if (String.isNotEmpty(preString) && String.isNotEmpty(postString)) {
            for (KBALayoutItem kbItem: kbaItems) {
                kbItem.value = String.isNotEmpty(kbItem.value) ? kbItem.value.replaceAll(preString, postString) : '';
                kbItem.labelName = String.isNotEmpty(kbItem.labelName) ? kbItem.labelName.replaceAll(preString, postString) : '';
                if (kbItem.subItems != null && !kbItem.subItems.isEmpty()) {
                    for (KBALayoutItem kbSubItem : kbItem.subItems) {
                        kbSubItem.value = String.isNotEmpty(kbSubItem.value) ? kbSubItem.value.replaceAll(preString, postString) : '';
                        kbSubItem.labelName = String.isNotEmpty(kbSubItem.labelName) ? kbSubItem.labelName.replaceAll(preString, postString) : '';
                    }
                }
            }
        }
    }

    private static Boolean isFollowing(String recordId) {
        return [Select Count() from EntitySubscription where parentId = :recordId and SubscriberId = :UserInfo.getUserId() LIMIT 1] >= 1;
    }

    @AuraEnabled
    public static void toggleFollow(String recordId, Boolean isFollowing) {
        if (isFollowing) {
            try {
                EntitySubscription followRec = [Select Id from EntitySubscription where parentId = :recordId and SubscriberId = :UserInfo.getUserId() LIMIT 1];
                Database.delete(followRec, true);
            } catch (QueryException qe) {
                //not actually following
            }
        } else {
            try {
                EntitySubscription follow = new EntitySubscription(
                        parentId = recordId, subscriberid = UserInfo.getUserId());
                insert follow;
            } catch (DmlException dml) {
                //already following
            }
        }
    }
}