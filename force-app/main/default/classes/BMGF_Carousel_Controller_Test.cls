@IsTest
private class BMGF_Carousel_Controller_Test {
    @isTest(SeeAllData=false)
    static void testCarousel() {

        CollaborationGroup cg = new CollaborationGroup(Name = 'Test Group', CollaborationType = 'Public');
        insert cg;

        CMS_Site__c cmsSite = new CMS_Site__c();
        cmsSite.Name = 'PS Community';
        insert cmsSite;

        CMS_Page_Group__c cmsGroup = new CMS_Page_Group__c();
        cmsGroup.Page_Group_Name__c = 'Test Group';
        cmsGroup.CMS_Site__c = cmsSite.Id;
        insert cmsGroup;

        CMS_Page_Group_Resource__c cmsGrpResource = new CMS_Page_Group_Resource__c();
        //cmsGrpResource = 'Test Resource';
        cmsGrpResource.Type__c = 'Feature Carousel';
        cmsGrpResource.External_url__c = '<img src="www.google.com"/>';
        cmsGrpResource.Caption__c = 'Google\'s photo';
        cmsGrpResource.CMS_Page_Group__c = cmsGroup.Id;
        insert cmsGrpResource;

        ContentVersion cv = new ContentVersion();
        cv.ContentURL = 'http://www.google.com/';
        cv.Title = 'Google.com';
        insert cv;

        cv = [Select ContentDocumentId, Id from ContentVersion where Id = :cv.Id];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cv.ContentDocumentId;
        cdl.LinkedEntityId = cmsGrpResource.Id;
        cdl.ShareType = 'I';

        insert cdl;

        BMGF_Carousel_Controller.CarouselContainer cc = BMGF_Carousel_Controller.getCarouselItems('', 'CMS_Page_Group__c', 'CMS_Page_Group_Resource__c', '', 'CMS_Page_Group__c', '', '', '', '', '', 'CMS_Page_Group_Resource__c.Caption__c',
                'CMS_Page_Group_Resource__c.External_URL__c', 'CreatedDate', false, 'Group_Name__c', '', '');

        for (BMGF_Carousel_Controller.CarouselParentItem cpi : cc.carouselParentItems) {
            for (BMGF_Carousel_Controller.CarouselItem ci : cpi.carouselItems) {
                System.assertEquals(ci.caption, cmsGrpResource.Caption__c);
                System.assert(ci.assetUrl.endsWith(cv.Id));
                System.assertEquals(ci.linkUrl, cmsGrpResource.External_URL__c);
            }
        }
        BMGF_Carousel_Controller.getCarouselItems('', 'CMS_Page_Group__c', 'CMS_Page_Group_Resource__c', '', 'CMS_Page_Group__c', '', 'CMS_Page_Group_Resource__c.External_URL__c', '', '', '', 'CMS_Page_Group_Resource__c.Caption__c',
                'CMS_Page_Group_Resource__c.External_URL__c', 'CreatedDate', false, 'Group_Name__c', '', '');
        for (BMGF_Carousel_Controller.CarouselParentItem cpi : cc.carouselParentItems) {
            for (BMGF_Carousel_Controller.CarouselItem ci : cpi.carouselItems) {
                System.assertEquals(ci.caption, cmsGrpResource.Caption__c);
                System.assert(ci.assetUrl.endsWith(cv.Id));
                System.assertEquals(ci.linkUrl, cmsGrpResource.External_URL__c);
            }
        }
        BMGF_Carousel_Controller.getCarouselItems(cg.Id, 'CMS_Page_Group__c', 'CMS_Page_Group_Resource__c', '', 'CMS_Page_Group__c', '', 'CMS_Page_Group_Resource__c.External_URL__c', '', '', '', 'CMS_Page_Group_Resource__c.Caption__c',
                'CMS_Page_Group_Resource__c.External_URL__c', 'CreatedDate', true, 'Page_Group_Name__c', '', '');
    }
}