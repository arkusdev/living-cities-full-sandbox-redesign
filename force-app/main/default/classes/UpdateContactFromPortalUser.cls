global class UpdateContactFromPortalUser { 
    @future 
    public static void updateContacts(String userId) {
        User u = [select ContactId,Email,FirstName,LastName,Title,Phone,Street,City,State,Country,PostalCode,Ask_Me_About__c
                    from User
                    where Id=:userId];

        if (u!=null && u.ContactId!=null) {
            Contact c = new Contact(Id=u.ContactId,Email=u.Email,FirstName=u.FirstName,LastName=u.LastName,Job_Title__c=u.Title,
            Phone=u.Phone,MailingStreet=u.Street,MailingCity=u.City,MailingState=u.State,MailingCountry=u.Country,MailingPostalCode=u.PostalCode,
            Ask_Me_About__c=u.Ask_Me_About__c);
            update c; 
        }
    }   
    
}