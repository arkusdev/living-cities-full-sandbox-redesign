@IsTest
public with sharing class Curators_Pick_Controller_Test {
    @IsTest
    static void testCuratorPicks() {
        CollaborationGroup cg = new CollaborationGroup(Name = 'Test Group', CollaborationType = 'Public');
        insert cg;
        CollaborationGroup cg2 = new CollaborationGroup(Name = 'Test Group2', CollaborationType = 'Private');
        insert cg2;
        
        CMS_Site__c psSite = new CMS_Site__c();
        psSite.Name = 'LC Community';
        insert psSite;

        CMS_Page_Group__c psGroup = new CMS_Page_Group__c();
        psGroup.Page_Group_Name__c = 'Test Group';
        psGroup.Curated_Topic_Name__c = 'Advising';
        psGroup.Show_Curated_Articles__c = true;
        psGroup.CMS_Site__c = psSite.Id;
        insert psGroup;

        PSArticle__kav newArticle = new PSArticle__kav(Title = 'test article', UrlName = 'testarticleurl',
                Language = 'en_US', Body__c = '<div class="striphtml">Content for Testing excludeme</div>',
                IsvisibleInPkb = true);
        insert newArticle;


        PSArticle__kav kav = [Select Id, KnowledgeArticleId from PSArticle__kav where id = :newArticle.Id];
        Topic newTopic = new Topic(Name = 'Advising');
        insert newTopic;

        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true);

        //insert new TopicAssignment(EntityId = newArticle.Id, TopicId = newTopic.Id);
        Curators_Pick_Controller.getCuratedArticles(cg.Id);
        Curators_Pick_Controller.getCuratedArticles(cg2.Id);
    }
}