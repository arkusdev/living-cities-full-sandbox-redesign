@isTest
public class UpdateContactFromPortalUser_Test{
    static testMethod void testUpdateContacts() {
        Test.startTest(); 
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
Account acc = new Account (
Name = 'newAcc1'
);  
insert acc;
Contact con = new Contact (
AccountId = acc.id,
FirstName = 'fname',
LastName = 'portalTestUser'
);
insert con;
User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
 
System.runAs ( thisUser ) {
//UserRole ur = [Select PortalType, PortalAccountId From UserRole where PortalType =:'CustomerPortal' and Name = 'Living Cities Customer User' limit 1];
Profile p = [select Id,name from Profile where UserType = 'PowerCustomerSuccess' limit 1];
 
User newUser = new User(
profileId = p.id,
username = 'newUser@yahoo.com',
email = 'pb@ff.com',
emailencodingkey = 'UTF-8',
localesidkey = 'en_US',
languagelocalekey = 'en_US',
timezonesidkey = 'America/Los_Angeles',
alias='nuser',
lastname='lastname',
firstname='fname',
contactId = con.id
);
insert newUser; 

        Contact c = [select FirstName from Contact where Id=:newUser.ContactId]; 
        System.assertEquals(c.FirstName,'fname'); 
  
newUser.email = 'test@ff.com';
update newUser;
}

        Test.stopTest(); 

       
    }
}