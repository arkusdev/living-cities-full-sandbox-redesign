@IsTest
private class BMGF_PortalUtils_Controller_Test {

    @IsTest
    static void testSingleField() {

        //run without data first

        String singleFieldValue = BMGF_PortalUtils_Controller.getSingleField(UserInfo.getUserId(), 'CMS_Site__c', 'Name');
        System.assertEquals(singleFieldValue, null);

        CMS_Site__c cmsSite = new CMS_Site__c();
        cmsSite.Name = 'LC';
        insert cmsSite;


        //run with value
        singleFieldValue = BMGF_PortalUtils_Controller.getSingleField(cmsSite.Id, 'CMS_Site__c', 'Name');
        System.assertEquals(singleFieldValue, cmsSite.Name);
        
    }
}