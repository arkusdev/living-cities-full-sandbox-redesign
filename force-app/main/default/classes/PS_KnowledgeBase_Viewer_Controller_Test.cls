@IsTest
private class PS_KnowledgeBase_Viewer_Controller_Test {
    static testMethod void testGetArticleType() {
        PSArticle__kav psArticle = new PSArticle__kav(
            Title = 'Only a test',
            UrlName = 'OnlyTest',
            Topics__c = 'City Government'
        );
        insert psArticle;

        String articleType = PS_KnowledgeBase_Viewer_Controller.getArticleType(psArticle.Id);
        System.assertEquals('PSArticle__kav', articleType, 'The article type is incorrect');
    }
    static testMethod void testObjectLayout() {
        PSSettings__c psSettings = new PSSettings__c();
        psSettings.KBA_URL_Rewrite_Internal__c = 'gatesfoundation-test';
        psSettings.KBA_URL_Rewrite_External__c = 'gatesfoundation-portal-test';
        insert psSettings;

        KBA_Custom_Layout__c cpl = new KBA_Custom_Layout__c();

        cpl.Name = 'Test Layout';
        cpl.Object_API_Name__c = 'PSTopics';

        insert cpl;

        List<KBA_Custom_Layout_Item__c> cpliList = new List<KBA_Custom_Layout_Item__c>();

        KBA_Custom_Layout_Item__c cpli = new KBA_Custom_Layout_Item__c();
        cpli.name = 'Topics Card';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Custom_Type__c = 'CommunityTopics';
        cpli.Custom_Label__c = 'Topics';

        cpliList.add(cpli);

        KBA_Custom_Layout_Item__c cplihtml = new KBA_Custom_Layout_Item__c();
        cplihtml.name = 'Topics Card';
        cplihtml.KBA_Custom_Layout__c = cpl.Id;
        cplihtml.Sequence__c = 2;
        cplihtml.Custom_HTML__c = '<hr/>';
        cpliList.add(cplihtml);

        insert cpliList;

        Test.startTest();
        PS_KnowledgeBase_Viewer_Controller.getLayoutByObject('PSTopics', '', 30);
        PS_KnowledgeBase_Viewer_Controller.getLayout('fake-urlname');
        Test.stopTest();
    }

    static testMethod void testKnowledgeArticleLayout() {
        PSSettings__c psSettings = new PSSettings__c();
        psSettings.KBA_URL_Rewrite_Internal__c = 'gatesfoundation-test';
        psSettings.KBA_URL_Rewrite_External__c = 'gatesfoundation-portal-test';
        insert psSettings;
        Topic newTopic = new Topic(Name = 'Test Topic');
        insert newTopic;
        PSArticle__kav newArticle = new PSArticle__kav(Title = 'test article', UrlName = 'testarticleurl', Language = 'en_US');
        insert newArticle;
        //insert new TopicAssignment(EntityId = newArticle.Id, TopicId = newTopic.Id);
        KBA_Custom_Layout__c cpl = new KBA_Custom_Layout__c();
        cpl.Name = 'Test Layout';
        cpl.Object_API_Name__c = 'PSArticle__kav';
        insert cpl;
        KBA_Custom_Layout_Item__c cpli = new KBA_Custom_Layout_Item__c();
        cpli.name = 'Topics Card';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Custom_Type__c = 'CommunityTopics';
        cpli.Custom_Label__c = 'Topics';
        cpli.Field_Icon__c = 'fa-test';
        insert cpli;
        cpli.clear();
        cpli.name = 'Left Side Title Card';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Column_Location__c = 'Left';
        cpli.Custom_Label__c = 'Title';
        cpli.Section_Header__c = true;
        cpli.Field_Icon__c = 'utility:file';
        insert cpli;
        KBA_Custom_Layout_SubItem__c cplsi = new KBA_Custom_Layout_SubItem__c();
        cplsi.name = 'Title Value';
        cplsi.Field_Name__c = 'Title';
        cplsi.Layout_Parent_Item__c = cpli.Id;
        cplsi.Field_Icon__c = 'fa-test';
        insert cplsi;
        cplsi.clear();
        cplsi.name = 'Title';
        cplsi.Layout_Parent_Item__c = cpli.Id;
        cplsi.Sequence__c = 1;
        cplsi.Field_Name__c = 'Title';
        cplsi.Custom_Type__c = 'FileField';
        insert cplsi;
        cpli.clear();
        cpli.name = 'Title';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Field_Name__c = 'Title';
        cpli.Field_Icon__c = 'http://somewhere.com/something.jpg';
        insert cpli;
        cpli.clear();
        cpli.name = 'Title';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Field_Name__c = 'Title';
        cpli.Custom_Type__c = 'FileField';
        insert cpli;
        cpli.clear();
        cpli.name = 'Title';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Custom_Type__c = 'articletopics';
        insert cpli;
        cpli.clear();
        cpli.name = 'Title';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Custom_Type__c = 'articlebytopiclist';
        insert cpli;
        Test.startTest();
        PS_KnowledgeBase_Viewer_Controller.KBALayout kbaLayout = new PS_KnowledgeBase_Viewer_Controller.KBALayout();
        kbaLayout.objectType = 'PSArticle__kav';
        kbaLayout.urlName = 'Test-Article';
        kbaLayout.recordId = newArticle.Id;
        PS_KnowledgeBase_Viewer_Controller.getLayoutDetails(kbaLayout);
        cpl = new KBA_Custom_Layout__c();
        cpl.Name = 'Test Layout';
        cpl.Object_API_Name__c = 'pskbtopicarticles';
        insert cpl;
        cpli = new KBA_Custom_Layout_Item__c();
        cpli.name = 'Title';
        cpli.KBA_Custom_Layout__c = cpl.Id;
        cpli.Sequence__c = 1;
        cpli.Custom_Type__c = 'articlebytopiclist';
        insert cpli;
        kbaLayout = new PS_KnowledgeBase_Viewer_Controller.KBALayout();
        kbaLayout.objectType = 'pskbtopicarticles';
        kbaLayout.urlName = '';
        kbaLayout.recordId = newTopic.Id;
        System.assert([Select Count() from Topic where Id = :kbaLayout.recordId] == 1);
        PS_KnowledgeBase_Viewer_Controller.getLayoutDetails(kbaLayout);
        PS_KnowledgeBase_Viewer_Controller.toggleFollow(kbaLayout.recordId,true);
        PS_KnowledgeBase_Viewer_Controller.toggleFollow(kbaLayout.recordId,false);
        Test.stopTest();
    }
    
}