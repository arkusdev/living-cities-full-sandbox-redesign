public with sharing class BMGF_PS_Home_Groups_Controller {

    public BMGF_PS_Home_Groups_Controller() {

    }

    @AuraEnabled
    public static List<AggregateResult> getMyGroupsInfo() {
        Set<Id> groupIds = new Set<Id>();

        for (CollaborationGroupMember cgm : [
                Select CollaborationGroupId
                from CollaborationGroupMember
                where MemberId = :UserInfo.getUserId()
        ]) {
            groupIds.add(cgm.CollaborationGroupId);
        }

        List<AggregateResult> arList = [
                Select Count(Id) MemberCount, CollaborationGroupId, CollaborationGroup.Name, CollaborationGroup.Owner.FirstName, CollaborationGroup.Owner.LastName
                from CollaborationGroupMember
                where CollaborationGroupId in :groupIds and NetworkId = :Network.getNetworkId()
                group by CollaborationGroupId, CollaborationGroup.Name, CollaborationGroup.Owner.FirstName, CollaborationGroup.Owner.LastName
                order by CollaborationGroup.Name
                limit 3
        ];

        return arList;
    }
}