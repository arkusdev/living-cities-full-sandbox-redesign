/**
 * Created by Robert on 1/31/2018.
 */

public with sharing class Curators_Pick_Controller {

    @AuraEnabled
    public static List<PSArticle__kav> getCuratedArticles(String groupId) {
        try {

            CollaborationGroup cGroup = [Select CollaborationType, Name from CollaborationGroup where Id = :groupId];
            if (cGroup.CollaborationType != 'Public') {
                Integer memberCount = [Select Count() from CollaborationGroupMember where MemberId = :UserInfo.getUserId() and CollaborationGroupId = :cGroup.Id];
                if (memberCount == 0) {
                    return null;
                }
            }

            CMS_Page_Group__c psGroup = [Select Show_Curated_Articles__c, Curated_Topic_Name__c from CMS_Page_Group__c where Page_Group_Name__c = :cGroup.Name LIMIT 1];
            Set<Id> topicEntitiesIds = new Set<Id>();

            for (TopicAssignment ta : [
                    Select EntityId
                    from TopicAssignment
                    where Topic.Name = :psGroup.Curated_Topic_Name__c
                    and EntityId in (Select Id from PSArticle__kav)
                    order by Entity.CreatedDate desc
                    LIMIT 1000
            ]) {
                topicEntitiesIds.add(ta.EntityId);
            }
            if (psGroup.Show_Curated_Articles__c) {
                return [
                        Select Id, Title, UrlName
                        from PSArticle__kav
                        where PublishStatus = 'Online'
                        and language = 'en_US'
                        and Id in :topicEntitiesIds
                        order by CreatedDate desc
                        limit 3
                ];
            }
        } catch (QueryException qe) {
            //missing group record returning null
        }
        return null;
    }
}