/**
 * Created by Robert on 10/18/2017.
 */

public with sharing class BMGF_PortalUtils_Controller {
    /**
    * @description retrieve a single field from an object
    * @param recordId       RecordId of object
    * @param objectName     Object we are querying
    * @param fieldName      Field we are retrieving
    * @return String
    */
    @AuraEnabled
    public static String getSingleField(String recordId, String objectName, String fieldName) {
        try {
            String queryString = 'Select ' + fieldName + ' from ' + objectName + ' where Id = :recordId';
            System.debug('queryString >>>>>>>> '+queryString);
            List<SObject> objList = Database.query(queryString);

            if (!objList.isEmpty()) {
                return String.valueOf(objList[0].get(fieldName));
            } else {
                return null;
            }
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}