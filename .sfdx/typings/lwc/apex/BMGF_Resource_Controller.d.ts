declare module "@salesforce/apex/BMGF_Resource_Controller.getResources" {
  export default function getResources(param: {recordId: any, parentObjectName: any, childObjectName: any, junctionObjectName: any, parentRelationshipField: any, childRelationshipField: any, parentSearchExpr: any, childSearchExpr: any, junctionSearchExpr: any, photoVideoUrlField: any, descriptionField: any, groupByFields: any, orderBy: any, isGroupPage: any, groupNameField: any}): Promise<any>;
}
