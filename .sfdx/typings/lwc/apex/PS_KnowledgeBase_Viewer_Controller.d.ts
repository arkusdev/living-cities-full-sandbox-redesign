declare module "@salesforce/apex/PS_KnowledgeBase_Viewer_Controller.getLayoutByObject" {
  export default function getLayoutByObject(param: {objectName: any, recordId: any, daysFromTodayNew: any}): Promise<any>;
}
declare module "@salesforce/apex/PS_KnowledgeBase_Viewer_Controller.getLayout" {
  export default function getLayout(param: {urlName: any}): Promise<any>;
}
declare module "@salesforce/apex/PS_KnowledgeBase_Viewer_Controller.getTopicArticles" {
  export default function getTopicArticles(param: {recordId: any, daysFromTodayNew: any, orderBy: any}): Promise<any>;
}
declare module "@salesforce/apex/PS_KnowledgeBase_Viewer_Controller.toggleFollow" {
  export default function toggleFollow(param: {recordId: any, isFollowing: any}): Promise<any>;
}
