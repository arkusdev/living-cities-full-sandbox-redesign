declare module "@salesforce/apex/BMGF_Carousel_Controller.getCarouselItems" {
  export default function getCarouselItems(param: {recordId: any, parentObjectName: any, childObjectName: any, junctionObjectName: any, parentRelationshipField: any, childRelationshipField: any, imageField: any, parentSearchExpr: any, childSearchExpr: any, junctionSearchExpr: any, captionField: any, linkField: any, orderBy: any, isGroupPage: any, groupNameField: any, isVideoField: any, isEmbeddedField: any}): Promise<any>;
}
