declare module "@salesforce/apex/Curators_Pick_Controller.getCuratedArticles" {
  export default function getCuratedArticles(param: {groupId: any}): Promise<any>;
}
