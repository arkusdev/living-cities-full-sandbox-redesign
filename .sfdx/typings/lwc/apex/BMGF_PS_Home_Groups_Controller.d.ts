declare module "@salesforce/apex/BMGF_PS_Home_Groups_Controller.getMyGroupsInfo" {
  export default function getMyGroupsInfo(): Promise<any>;
}
