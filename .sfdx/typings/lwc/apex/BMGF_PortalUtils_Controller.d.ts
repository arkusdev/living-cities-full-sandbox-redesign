declare module "@salesforce/apex/BMGF_PortalUtils_Controller.getSingleField" {
  export default function getSingleField(param: {recordId: any, objectName: any, fieldName: any}): Promise<any>;
}
